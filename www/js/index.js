"use strict";
var app = {
	apiURL: "https://api.white-label-dev.co.uk/loyaltyapp/api.php?",
	apiToken: "test",
	currentUser: {},
	sectionCurrent: null,
	sectionInit: [],
	uuid: 0,
	lookupKey: "UUID",
	initialise: function () {
		//document.addEventListener ("load", function () {}, false);
		//document.addEventListener ("pause", function () {}, false);
		//document.addEventListener ("resume", function () {}, false);
		//document.addEventListener ("online", function () {}, false);
		//document.addEventListener ("offline", function () {}, false);

		document.addEventListener("deviceready", this.onDeviceReady, false);
	},

	onDeviceReady: function () {


		document.addEventListener("backbutton", function () {
			app.quit();
			/*if (app.sectionCurrent == null || app.sectionCurrent == "choose") {
				
			} else{
				app.sectionShow("choose");
			}*/
		}, false);


		if (window.localStorage.getItem(app.lookupKey)) {
			var user = JSON.parse(window.localStorage.getItem("user"));
			app.uuid = window.localStorage.getItem(app.lookupKey);
			var user = JSON.parse(window.localStorage.getItem("user"));
			if (user) {
				app.currentUser = user;
				app.sectionShow("load");
			} else {
				app.sectionShow("choose");
			}
		} else {
			app.uuid = app.genUUID();
			window.localStorage.setItem(app.lookupKey, app.uuid);
			app.sectionShow("choose")
		}
	},
	quit:function(){
		if (navigator && navigator.app)
					navigator.app.exitApp();
	},
	ajax: function (requestAPI, requestMethod, requestData, requestCallback) {
		$.support.cors = true;
		$.ajax({
			url: app.apiURL + requestAPI,
			method: requestMethod || "GET",
			headers: { "Authorization": "Bearer " + app.apiToken },
			timeout: 15000,
			crossDomain: true,
			scriptCharset: "UTF-8",
			data: requestData,
			dataType: "json",
			success: requestCallback,
			error: requestCallback,
		});
	},
	genUUID: function () {
		var now = new Date().getTime();
		var uuid = "########-####-4###-@###-############".replace(/[#@]/g, function (char) {
			var rand = (now + Math.random() * 16) % 16 | 0;
			now = Math.floor(now / 16);
			return (char == "#" ? rand : (rand & 0x3 | 0x8)).toString(16);
		});

		return uuid;
	},
	alert: function (title, message, buttonName) {
		navigator.notification.confirm(message, null, title, buttonName || "OK");
	},
	login: function (user, cb) {
		$('#loginForm button#login_btn').hide();
		$('#loginForm button.spinner').show();
		$('#loginForm a img').hide();
		app.ajax("command=login&uuid=" + app.uuid, "POST", user, function (xhr, description) {

			if (xhr.success) {
				app.currentUser.name = xhr.name;
				window.localStorage.setItem("user", JSON.stringify(user));
				app.getPoints();
			} else {
				app.alert("Error login", xhr.responseJSON.message)
				$('#loginForm button#login_btn').show();
				$('#loginForm button.spinner').hide();
				$('#loginForm a img').show();
			}
		});
	},
	register: function (user) {
		$('#registerForm button#register_btn').hide();
		$('#registerForm button.spinner').show();
		$('#registerForm a img').hide();
		app.ajax("command=register&uuid=" + app.uuid, "POST", user, function (xhr, description) {
			if (xhr.success) {
				//app.currentUser.name = xhr.name;	
				window.localStorage.setItem("user", JSON.stringify(user));
				app.getPoints();
			} else
				app.alert("Error register", xhr.responseJSON.message)

			$('#registerForm button#register_btn').show();
			$('#registerForm button.spinner').hide();
			$('#registerForm a img').show();
		});
	},
	resetPassword: function (email) {
		$('section#sectionlogin .overlay').show();
		app.ajax("command=reset&email="+email+"&uuid="+app.uuid,"GET",null,function(xhr,description){
			console.log(xhr)
			$('section#sectionlogin .overlay').hide();
			if(xhr.success){
				app.alert("Password reset",xhr.message);
			}else{
				app.alert("Password reset","Failed to reset password, please try again.")
			}
		});
	},
	generateQRCode: function () {
		var qrcode = new QRCode("qrcode", {
			width: 200,
			height: 200,
			colorDark: "#000000",
			colorLight: "#ffffff",
			correctLevel: QRCode.CorrectLevel.L
		});
		qrcode.makeCode(app.uuid);
	},
	getPoints: function () {
		app.ajax("command=points&uuid=3d2f28e9-fda9-4139-9fdb-1b2103cb1bd1", "GET", null, function (xhr, description) {
			if (xhr.success) {
				app.points = xhr.points;
				app.sectionShow("main");
			}
		});
	},
	sectionShow: function (sectionName, sectionAction) {
		if (sectionName != "main")
			$('header').hide();

		var sectionID = "#section" + sectionName;
		var sectionInit = (app.sectionInit.indexOf(sectionName) > -1 ? false : true);
		if (sectionInit)
			app.sectionInit.push(sectionName);

		if (app.sectionCurrent != null) {
			$("#section" + app.sectionCurrent).addClass("hide");
			$(window).scrollTop(0);
		}
		app.sectionCurrent = sectionName;
		$(sectionID).removeClass("hide");

		sectionName += "Section";
		return (app[sectionName] ? app[sectionName](sectionInit, sectionAction) : true);
	},
	loginSection: function (sectionInit, sectionAction) {
		if (sectionInit) {
			$('form#loginForm a img').on("click", function (e) {
				app.sectionShow('choose')
			});
			$('form#loginForm span.psw a').click(function () {
				var email = $("form#loginForm input#loginEmail").val();
				var regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				if (email) {
					if(regExp.test(email)){
						app.resetPassword(email);
					}else{
						app.alert("Password reset","Email not valid!");
						$("form#loginForm input#loginEmail").focus();
					}
				} else {
					//alert the user to type their email in the form
					app.alert("Password reset","Type your email in the form and click the link again!");
				}
			});
			$('#loginForm').submit(function (e) {
				e.preventDefault();
				var user = {};
				user.email = $('#loginEmail').val();
				user.password = $('#loginPassword').val();
				if (user.password.length < 8)
					app.alert("Error password", "Password should be at least 8 characters");
				else
					app.login(user);
			});
		}
	},
	registerSection: function (sectionInit, sectionAction) {
		if (sectionInit) {
			$('form#registerForm a img').on("click", function (e) {
				app.sectionShow('choose')
			});

			$('#registerForm').submit(function (e) {
				e.preventDefault();
				var user = {};
				user.email = $('#register_email').val();
				user.password = $('#register_password').val();
				user.name = $('#register_name').val();
				app.currentUser = user;

				app.register(user);
			});
		}
	},
	chooseSection: function (sectionInit) {
		if (sectionInit) {
			$("#sectionchoose button.choose_register").on("click", function () {
				app.sectionShow("register");
			});
			$("#sectionchoose button.choose_login").on("click", function () {
				app.sectionShow("login");
			});
		}
	},
	mainSection: function (sectionInit, sectionAction) {
		if (sectionInit) {
			var buttons = document.getElementById("buttons").getElementsByTagName("button");
			for (var i = 0; i < buttons.length; ++i) {
				buttons[i].addEventListener("click", function () {
					document.getElementById("inner").style.left = this.dataset.x + "%";
					return false;
				});
			}
			app.generateQRCode();
		}


		var user = app.currentUser;
		var time_msg = "";
		var now = (new Date()).getHours();;
		if (now < 12 && now >= 12)
			time_msg = "Good Morning";
		else if (now < 18 && now > 12)
			time_msg = "Good day";
		else
			time_msg = "Good evening";

		$('#welcome_message').text(time_msg + " " + user.name + " , you've got " + app.points + " points!");
	},
	loadSection: function (sectionInit) {
		app.login(app.currentUser);
	}
};

app.initialise();
